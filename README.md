# JWT-Auth

Klausurersatzleistung für "Verteilte Systeme"

## Bsp-Nutzer

Es wurden die Nutzer mit den folgenden Attributen in der Datenbank eingetragen:
username="ChefBesos", email="jeffbezos@gmail.com", password="Passwort123!"
username="EronMisk", email="eronmisk@gmail.com", password="Passwort123!"
username="billyShorts", email="billgates@gmail.com", password="Passwort123!"
username="Max", email="max@gmail.com", password="Passwort123!"
username="femu", email="demo@test.de", password="Test"


## Ausführen

Um die Implementierung auszuführen, ist nur die Docker-Compose Datei benötigt. Mithilfe des Commands "docker compose up" kann die Authorisierungsarchitektur erstellt werden. Erreichbar und Sichtbar wird die Implementierung über das Ausrufen des Links: 127.0.0.1:5000

## Verwendung

[POST] signin: Anfrage mit den Attributen Email und Passwort um einen validen JWT zu bekommen
[POST] verify-token: Anfrage mit dem Token - falls valider Token als antwort eine Nachricht

Im Falle von MicroServices, die eine Authorisierung benötigen würden, müsste im Mikroservice als erstes die Funktion verifyToken(auth_token) aus app.service.auth_service ausgeführt werden. Im Falle eines invaliden Tokens würde die API automatisch eine 404 Error message antworten.

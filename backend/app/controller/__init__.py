from flask import Blueprint
from flask_restx import Api
from .user import api as user_ns


blueprint = Blueprint("api", __name__)

api = Api(
    blueprint, title="JWT Implementierung", version="1.1", description="Klausurersatzleistung für Verteilte Systeme"
)

api.add_namespace(user_ns, path="/users")



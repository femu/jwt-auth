from flask_restx import Resource
from flask import request
from app.service.user_service import *
from app.service.auth_service import verifyToken
from app.util.user_dto import UserDto
from flask_cors import  cross_origin

api = UserDto.api
_user = UserDto.model
_username = UserDto.model_username
_login = UserDto.model_login
_token = UserDto.model_token
_signup = UserDto.model_signup

auth_parser = api.parser()
auth_parser.add_argument('Authorization', location='headers')


@api.route('/signup')
class SignUp(Resource):
    @api.doc('Signup')
    @api.expect(_signup, envelope='data')
    @api.response(404, 'Errors with SignUp')
    def post(self):
        data = request.json
        try:
            accounts = data["accounts"]
        except:
            accounts = None
        return register(username=data["username"], email=data["email"], lastName=data["lastName"], firstName=data["firstName"],
                 password=data["password"])



@api.route('/usernames')
class Usernames(Resource):
    @api.doc('Usernames')
    # @api.marshal_list_with(envelope='data')
    @api.response(404, 'Errors with Usernames')
    def get(self):
        usernames = get_all_usernames()
        return {"data": usernames}


@api.route('/signin')
class Login(Resource):
    @api.doc('Signin')
    @api.expect(_login, validate=True)
    @api.response(404, 'Wrong credentials or user not found')
    # @api.marshal_with(_token, envelope='data')
    @cross_origin(supports_credentials=True)
    def post(self):
        email = request.json["email"]
        password = request.json["password"]
        token, username, user_id = login(email, password)
        return {"message": "Successfully authenticated", "username": username, "user_id": user_id}, 201, {"Authorization": token,
                                                                                      "Access-Control-Expose-Headers": "Authorization"}


@api.route('/verify-token')
class VerifyToken(Resource):
    @api.expect(auth_parser, validate=True)
    @api.doc('Verifies Token')
    def post(self):
        token = auth_parser.parse_args()['Authorization']
        if verifyToken(token):
            response_object = {
                'status': 'success',
                'message': 'Token is valid'
            }
            return response_object, 201


from app.config import TokenConfig
import datetime
import jwt
from flask_restx import abort
from app.model.user import User


def secretKey():
    secret = TokenConfig.SECRET_TOKEN_KEY  # reading the Secret_Token_Key from the .env file
    return secret


def encodeToken(uniqueAttribute, expirationDays=1, expirationMinutes=0, ExpirationSeconds=0):
    secret = secretKey()
    try:
        payload = {
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=expirationDays, minutes=expirationMinutes,
                                                                   seconds=ExpirationSeconds),
            # expiration date of the token
            'iat': datetime.datetime.utcnow(),  # time token is generated
            'sub': uniqueAttribute  # unique attribute which identifies User
        }
        return jwt.encode(payload, secret,
                          algorithm='HS256')  # encodes a token based on the HS256 algorithm with the given attributes

    except Exception as e:
        print(e)  # needed so the CLI/ Docker container shows the output
        return e


def decodeToken(token):
    secret = secretKey()
    return jwt.decode_complete(token, secret, algorithms="HS256")


def verifyToken(auth_token):
    if auth_token is None:
        abort(404, message="No token provided in Header")
    try:
        secret = secretKey()
        payload = jwt.decode(auth_token, secret, algorithms="HS256")  # throws error if token is somehow invalid
        return True
    except jwt.exceptions.ExpiredSignatureError:
        abort(404, message="Token expired")
    except jwt.exceptions.InvalidSignatureError:
        abort(404, message="Token invalid")
    except jwt.exceptions.InvalidTokenError:
        abort(404, message="Token error")
    except:
        abort(404, message="Something went wrong")


def getUserFromToken(auth_token):
    verifyToken(auth_token)
    payload = jwt.decode(auth_token, secret, algorithms="HS256")
    uniqueAttribute = payload["sub"]
    user = User.query.filter_by(email=uniqueAttribute).first()
    if user is None:
        abort(404, message="User not found")
    return user

from flask import abort
from .auth_service import encodeToken
from app.model.user import User
from app import bcrypt, db

def get_all_usernames():
    users = User.query.with_entities(User.username).all()
    user_list = []
    for a in users:
        for b in a:
            user_list.append(b)
    return user_list

def get_all_users():
    users = User.query.with_entities(User.username).all()
    return users


def get_user_by_email(email):
    user = User.query.filter_by(email=email).first()
    if user is None:
        abort(400, 'User not found')
    return user


def register(username, email, lastName, firstName, password):
    if User.query.filter_by(username=username).first() is not None or User.query.filter_by(
            email=email).first() is not None:
        abort(400, 'User already exist')

    hashed_password = bcrypt.generate_password_hash(password, 10)
    user = User(username=username, email=email, lastName=lastName, firstName=firstName,
                password=hashed_password)
    db.session.add(user)
    db.session.commit()

    response_object = {
        'status': 'success',
        'message': 'Successfully created',
        'user_id': user.user_id
    }
    return response_object, 201


def login(email, password):
    user = get_user_by_email(email)
    password_matched_email = user.password
    if not bcrypt.check_password_hash(password_matched_email, password):
        abort(400, 'Wrong credentials')
    token = encodeToken(email)
    return token, user.username, user.user_id
